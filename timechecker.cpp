#include <stdio.h>
#include <Windows.h>

int main()
{

	int st,et;
	int res;
	char file_name[50];
	char op[70];
	FILE *fp = fopen("list.txt","r");
	FILE *fo = fopen("result.txt","w");
	while( fscanf(fp,"%s",file_name) != EOF )
	{
		st = GetTickCount();
		strcpy(op,"scp.exe < ");
		strcat(op,file_name);

		st = GetTickCount();
		system(op);
		et = GetTickCount();
		FILE *foo = fopen("output.txt","r");
		fscanf(foo,"%d",&res);
		if(res==0)
		{
			printf("%s: TLE\n",file_name);
			fprintf(fo,"%s: TLE\n",file_name);
		}
		else 
		{
			printf("%s: %d ms\n",file_name,et-st);
			fprintf(fo,"%s: %d ms\n",file_name,et-st);
		}
		fclose(foo);
	}
	fclose(fp);
	fclose(fo);
}