#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <time.h>
#include <algorithm>


#define INPUT_FILE "s-k-50-95"
#define INF 0x7FFFFFFF
#define INSANE

#ifdef INSANE
int insane_sets[300][750];
int isc[300];

int insane_ans[300];
int ian;


clock_t st, et;
#endif

// Global
int M;
int N; 
int Mdiv32;

int MIN = INF;
int MAX_M;
int covered[23];
int sets[300][23];

struct H
{
	int num;
	int list[300],list_num;
}h[750];

bool compare(const H & l, const H & r)
{
	return l.list_num < r.list_num;
}


void add_element(int idx, int num)
{
	h[num].list[h[num].list_num++] = idx;
	sets[idx][num/32] |= (1<<(num%32));
}

void input()
{
	int i,j,offset;
	char BUF[1024];
	FILE *fp = stdin;
	//FILE *fp = fopen(INPUT_FILE,"r");
	if ( fp == NULL )
	{
		printf("fopen failed.\n");
		exit(0);
	}
	fscanf(fp,"%d\n",&M);
	fscanf(fp,"%d\n",&N);
	Mdiv32 = M/32+1;
	for(i=0;i<N;i++)
	{
		int temp;
		char *pBUF;
		fgets(BUF,1024,fp);
		pBUF = BUF;
		
		j=0;
		while( sscanf(pBUF,"%d%n",&temp,&offset) == 1 )
		{
#ifdef INSANE
			insane_sets[i][isc[i]++]=temp;
#endif
			add_element(i,temp-1);
			pBUF += offset;
			j++;
		}
		if( MAX_M < j ) MAX_M = j;
	}
	for(i=0;i<M;i++)
	{
		h[i].num = i;
		covered[i/32] |= (1<<i%32);
	}

	std::sort(h,h+M,compare); // order
}

int is_covered(int a[])
{
	int i;
	for(i=0;i<Mdiv32;i++)
	{
		if( a[i] != covered[i] ) return 0;
	}
	return 1;
}
void calc_assign(int a[], int b[])
{
	for(int i=0;i<Mdiv32;i++)
	{
		a[i] = b[i];
	}
}
void calc_or(int a[], int b[])
{
	for(int i=0;i<Mdiv32;i++)
	{
		a[i] |= b[i];
	}
} 

/////////////////////////////////////// greedy /////////////////////////////////////
int calc_diff_f(int a, int b)
{
	int cnt = 0;
	for(int i=0;i<32;i++)
	{
		if( (1 & (a>>i)) == 0 &&  (1 & (b>>i)) == 1 ) cnt++;
	}
	return cnt;
}

int calc_diff(int a[], int b[])
{
	int i,sum=0;
	for(i=0;i<Mdiv32;i++)
	{
		sum += calc_diff_f(a[i],b[i]);
	}
	return sum;
}

void process1(int loop)
{
	int lp,llp,tt;
	int ans[23];
	int chk[300];
	int tmp_insane_ans[300];
	int tmp_ian;
	int diff,i;
	int max_diff,pos;
	int cover = 0;
	for(lp=-1;lp<loop;lp++)
	{
	
		for(llp=lp+1;llp<loop;llp++)
		{
			cover = 0;
			memset(ans,0,sizeof(ans));
			memset(chk,0,sizeof(chk));
			tmp_ian = 0;
			if(lp>=0)
			{
				pos = lp;
				chk[pos] = 1;
				calc_or(ans,sets[pos]);
				tmp_insane_ans[tmp_ian++] = pos;
				cover++;
			}
			if(llp>=1)
			{
				pos = llp;
				chk[pos] = 1;
				calc_or(ans,sets[pos]);
				tmp_insane_ans[tmp_ian++] = pos;
				cover++;
			}

			for(;;)
			{
				max_diff = 0;
				for(i=0;i<N;i++)
				{
					if(chk[i])continue;
					diff = calc_diff(ans,sets[i]);
					if( max_diff < diff )
					{
						pos = i;
						max_diff = diff;
					}
				}
				if(max_diff == 0)break;
				calc_or(ans,sets[pos]);
				tmp_insane_ans[tmp_ian++] = pos;
				cover++;
			}
			if( MIN > cover )
			{
				for(tt=0;tt<tmp_ian;tt++)
				{
					insane_ans[tt] = tmp_insane_ans[tt];
				}
				ian = tmp_ian;

				MIN = cover;
			}
		}
	}
	
}
////////////////////////////////////////////////////////////////////////////////








///////////////////////////// backtracking ////////////////////////////////////
struct stack
{
	int lvl;
	int now[23];
	int i;
	int i_pos;
	int isp;
	int *order;
	int op;

#ifdef INSANE
	int sel;
#endif

}stacks[300]; // max depth : 300

int is_not_coverd(int bits[], int pos)
{
	int res = bits[pos/32] & (1<<(pos%32));
	if( res == 0 ) return 1;
	return 0;
}

int get_order(stack* s) // cutting.. 
{
	int i;
	for(i=s->isp;i<M;i++)
	{
		if( is_not_coverd(s->now,h[i].num) )
		{
			s->order = h[i].list;
			s->op = h[i].list_num;
			s->isp = i+1;
			break;
		}
	}
	return 0;
}

void process2()
{
	int covered_flag;
	int top=1;
	get_order(&stacks[0]);

#ifdef INSANE
    double t;
#endif


	for(;;)
	{
#ifdef INSANE
        et = clock();
        t = (double)(et-st) / CLOCKS_PER_SEC;
        if( t > 59.8 )
        {
            break;
        }
#endif

		if( stacks[top-1].i>=stacks[top-1].op)
		{
			goto POP;
		}

		
		stacks[top].lvl=stacks[top-1].lvl+1;
		stacks[top].isp=stacks[top-1].isp;
		calc_assign(stacks[top].now,stacks[top-1].now);
		calc_or(stacks[top].now,sets[stacks[top-1].order[stacks[top-1].i]]);
		get_order(&stacks[top]);
#ifdef INSANE
		stacks[top-1].sel = stacks[top-1].order[stacks[top-1].i];
#endif
		stacks[top-1].i++;
		top++;
		
		covered_flag = is_covered(stacks[top-1].now);
		if( covered_flag || stacks[top-1].lvl >= MIN-1  )
		{
			if( covered_flag && MIN > stacks[top-1].lvl ) 
			{
				MIN = stacks[top-1].lvl;
#ifdef INSANE
				// trace..
				int xx;
				xx = top-2;
				ian = 0;
				while(xx>=0)
				{
					insane_ans[ian++] = stacks[xx--].sel;
				}
#endif
			}
			// pop
			POP:
			stacks[top-1].i = 0;
			stacks[top-1].op = 0;
			top--;
			if( top == 0) break;
		}
	}
}
///////////////////////////////////////////////////////////////////////////////



void output()
{
	/*
	FILE *fp = fopen("output.txt","w");
	fprintf(fp,"%d\n",MIN);
	*/
	printf("%d\n",MIN);
	/*
#ifdef INSANE
	for(int i=0;i<ian;i++)
	{
		for(int j=0;j<isc[insane_ans[i]];j++)
		{
			printf("%d ",insane_sets[insane_ans[i]][j]);
		}
		printf("\n");
	}
#endif
	*/
}


int main()
{
	st = clock();
	input();
	process1(N); // greedy
	process2(); // backtracking
	output();
	return 0;
}