#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <algorithm>

#define INPUT_FILE "s-k-50-95"
#define INF 0x7FFFFFFF

// Global
int M;
int N; 
int Mdiv32;

int MIN = INF;
int MAX_M;
int covered[23];
int sets[300][23];
struct H
{
	int num;
	int list[300],list_num;
}h[750];

bool compare(const H & l, const H & r)
{
	return l.list_num < r.list_num;
}


void add_element(int idx, int num)
{
	h[num].list[h[num].list_num++] = idx;
	sets[idx][num/32] |= (1<<(num%32));
}

void input()
{
	int i,j,offset;
	char BUF[1024];
	FILE *fp = stdin;
	//FILE *fp = fopen(INPUT_FILE,"r");
	if ( fp == NULL )
	{
		printf("fopen failed.\n");
		exit(0);
	}
	fscanf(fp,"%d\n",&M);
	fscanf(fp,"%d\n",&N);
	Mdiv32 = M/32+1;
	for(i=0;i<N;i++)
	{
		int temp;
		char *pBUF;
		fgets(BUF,1024,fp);
		pBUF = BUF;
		
		j=0;
		while( sscanf(pBUF,"%d%n",&temp,&offset) == 1 )
		{
			add_element(i,temp-1);
			pBUF += offset;
			j++;
		}
		if( MAX_M < j ) MAX_M = j;
	}
	for(i=0;i<M;i++)
	{
		h[i].num = i;
		covered[i/32] |= (1<<i%32);
	}

	std::sort(h,h+M,compare); // order
}

int is_covered(int a[])
{
	int i;
	for(i=0;i<Mdiv32;i++)
	{
		if( a[i] != covered[i] ) return 0;
	}
	return 1;
}
void calc_assign(int a[], int b[])
{
	for(int i=0;i<Mdiv32;i++)
	{
		a[i] = b[i];
	}
}
void calc_or(int a[], int b[])
{
	for(int i=0;i<Mdiv32;i++)
	{
		a[i] |= b[i];
	}
} 

/////////////////////////////////////// greedy /////////////////////////////////////
int calc_diff_f(int a, int b)
{
	int cnt = 0;
	for(int i=0;i<32;i++)
	{
		if( (1 & (a>>i)) == 0 &&  (1 & (b>>i)) == 1 ) cnt++;
	}
	return cnt;
}

int calc_diff(int a[], int b[])
{
	int i,sum=0;
	for(i=0;i<Mdiv32;i++)
	{
		sum += calc_diff_f(a[i],b[i]);
	}
	return sum;
}

void process1()
{
	int ans[23];
	int chk[300];
	int diff,i;
	int max_diff,pos;
	int cover = 0;
	memset(ans,0,sizeof(ans));
	memset(chk,0,sizeof(chk));
	for(;;)
	{
		max_diff = 0;
		for(i=0;i<N;i++)
		{
			if(chk[i])continue;
			diff = calc_diff(ans,sets[i]);
			if( max_diff < diff )
			{
				pos = i;
				max_diff = diff;
			}
		}
		if(max_diff == 0)break;
		calc_or(ans,sets[pos]);
		chk[pos]=1;
		cover++;
	}
	MIN = cover;
}
////////////////////////////////////////////////////////////////////////////////








///////////////////////////// backtracking ////////////////////////////////////
struct stack
{
	int lvl;
	int now[23];
	int i;
	int i_pos;
	int isp;
	int *order;
	int op;

}stacks[300]; // max depth : 300

int is_not_coverd(int bits[], int pos)
{
	int res = bits[pos/32] & (1<<(pos%32));
	if( res == 0 ) return 1;
	return 0;
}

int get_order(stack* s) // cutting.. 
{
	int i;
	for(i=s->isp;i<M;i++)
	{
		if( is_not_coverd(s->now,h[i].num) )
		{
			s->order = h[i].list;
			s->op = h[i].list_num;
			s->isp = i+1;
			break;
		}
	}
	return 0;
}

void process2()
{
	int covered_flag;
	int top=1;
	get_order(&stacks[0]);
	for(;;)
	{
		if( stacks[top-1].i>=stacks[top-1].op)
		{
			goto POP;
		}

		
		stacks[top].lvl=stacks[top-1].lvl+1;
		stacks[top].isp=stacks[top-1].isp;
		calc_assign(stacks[top].now,stacks[top-1].now);
		calc_or(stacks[top].now,sets[stacks[top-1].order[stacks[top-1].i]]);
		get_order(&stacks[top]);
		
		
		stacks[top-1].i++;
		top++;
		
		covered_flag = is_covered(stacks[top-1].now);
		if( covered_flag || stacks[top-1].lvl >= MIN-1  )
		{
			if( covered_flag && MIN > stacks[top-1].lvl ) 
			{
				MIN = stacks[top-1].lvl;
			}
			// pop
			POP:
			stacks[top-1].i = 0;
			stacks[top-1].op = 0;
			top--;
			if( top == 0) break;
		}
	}
}
///////////////////////////////////////////////////////////////////////////////



void output()
{
	/*
	FILE *fp = fopen("output.txt","w");
	fprintf(fp,"%d\n",MIN);
	*/
	printf("%d\n",MIN);
}

int main()
{
	input();
	//process1(); // greedy
	process2(); // backtracking
	output();
	return 0;
}